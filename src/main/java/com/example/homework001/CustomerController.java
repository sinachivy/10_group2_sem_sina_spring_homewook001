package com.example.homework001;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;


@RestController
@RequestMapping("/api/v1")
public class CustomerController {
    ArrayList<Customer> customers=new ArrayList<>();
    public CustomerController(){
        customers.add(new Customer("sina","female",20,"Takeo" ));
        customers.add(new Customer("Titya","Male",16,"Takeo"));
        customers.add(new Customer("Komsan","Male",10,"Takeo"));
    }
    //Get customer
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomer(){
        return customers;
    }

    //insert into list
  @PostMapping("/customers")
    public Customer insert(@RequestBody Customer customer){
        customers.add(customer);
        return customer;
  }

  // get customer by id
    @GetMapping("/customers/{customerID}")
    public Customer getCustomerById(@PathVariable("customerID") Integer cusid ){
        for(Customer cus:customers){
            if(cus.getId()==cusid){
               return cus;
            }
        }
        return null;
    }
//find customer using requset param
    @GetMapping("/customers/search")
    //requestParam use to find or pigination
    public Customer findCustomerByID(@RequestParam String name){
        for (Customer cus:customers){
            if(cus.getName() .equals(name) ){
                return cus;
            }
        }
        return null;
    }
@PutMapping("/customer/{id}")
public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody Customer customer ) {
    for (int i = 0; i < customers.size(); i++) {
        if (customers.get(i).getId() == id) {
            customers.get(i).setName(customer.getName());
            customers.get(i).setGender(customer.getGender());
            customers.get(i).setAge(customer.getAge());
            customers.get(i).setAddress(customer.getAddress());
            return ResponseEntity.ok(customer);
        }

    }
    return ResponseEntity.notFound().build();
}
    @DeleteMapping("/customers/{customerId}")
    public  ResponseEntity<?> deleteCustomer(@PathVariable("customerId") int id){
//        Map<String,Object>
        for (int i=0;i<customers.size();i++){
            if (customers.get(i).getId()==id){
                customers.remove(i);
                return ResponseEntity.ok(customers);
            }
        }
        return ResponseEntity.notFound().build();
    }

}
