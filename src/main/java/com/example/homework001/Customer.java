package com.example.homework001;

public class Customer {
    private int id;
    private static int cusID = 1;


    private String name;
    private String gender;
    private int age;
    private String address;

    public Customer( String name, String gender, int age, String address) {
        this.id = cusID++;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCusID() {
        return cusID;
    }

    public static void setCusID(int cusID) {
        Customer.cusID=cusID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}


